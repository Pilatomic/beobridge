#include "datalink.h"
#include "driver/gpio.h"
#include "driver/timer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_avrc_api.h"
#include "esp_rom_sys.h"


#define DEBUG_TAG                   "DATALINK"
#define CMDLIST_END                 (0x00)
#define WAIT_NO_RX_BIT_BEFORE_TX    (8)
#define WAIT_BIT_ON_COLLISION       (7)
#define COLLISION_BYTE              (0x00)
#define DELAY_WRITE_READ_PIN_US     (50) //Lower values means no output ! WTF

#define DEBUG_STATES
//#define TX_DEBUG_PATTERN

#define TIMER_ISR_BIT_CLR() TIMERG0.int_clr_timers.t0=1

static const gpio_num_t datalink_pin = 25;
static const timer_group_t datalink_tmr_grp = 0;
static const timer_idx_t datalink_tmr_idx = 0;
static const uint32_t datalink_tmr_div = 80;
static const uint64_t datalink_ticks_full_bit = 3125;
static const uint64_t datalink_ticks_half_bit = 1562;

typedef enum
{
    STATE_IDLE = 0,
    STATE_RX,
    STATE_TX,
    STATE_TX_ABORTED,
} AsynchronousStates_t;

static const char* stateStrings[] =
{
    "Idle", "Rx", "Tx", "TxAbort"
};

typedef enum
{
    FROM_ALARM_VALUE,
    FROM_COUNTER_VALUE,
} AlarmRescheduleType_t;

typedef struct
{
    uint8_t byte;
    uint16_t deltaT;
} ByteInfo_t;

static ByteInfo_t rxByteInfo;
static uint32_t bitCount;
static uint8_t txBuffer;

static volatile AsynchronousStates_t s_state;
static xQueueHandle s_datalink_rx_queue = NULL;
static xQueueHandle s_datalink_tx_queue = NULL;


#ifdef DEBUG_STATES
static xQueueHandle s_debug_states_queue = NULL;
#endif

static void rescheduleAlarm(AlarmRescheduleType_t type, uint64_t delay)
{
    uint64_t alarmValue;

    switch(type)
    {
    case FROM_ALARM_VALUE:
        timer_get_alarm_value(datalink_tmr_grp, datalink_tmr_idx,
                              &alarmValue);
        break;
    case FROM_COUNTER_VALUE:
        timer_get_counter_value(datalink_tmr_grp, datalink_tmr_idx,
                                &alarmValue);
        break;
    default:
        return;
    }

    alarmValue += delay;
    timer_set_alarm_value(datalink_tmr_grp, datalink_tmr_idx, alarmValue);
    timer_set_alarm(datalink_tmr_grp, datalink_tmr_idx, TIMER_ALARM_EN);
}

static void enterState(AsynchronousStates_t state)
{
    switch(state)
    {
    case STATE_TX:
        gpio_intr_disable(datalink_pin);
        break;

    case STATE_IDLE:
        gpio_intr_enable(datalink_pin);
        break;

    default:
        break;
    }
    s_state = state;

#ifdef DEBUG_STATES
    xQueueSendToBackFromISR(s_debug_states_queue, &state, NULL);
#endif
}

static void datalink_pin_isr(void* arg)
{
    //Detect incoming transmission
    if(s_state == STATE_IDLE) enterState(STATE_RX);

    //Maintain bit sync
    if(s_state == STATE_RX) rescheduleAlarm(FROM_COUNTER_VALUE, datalink_ticks_half_bit);
}

static void onCollisionDetected()
{
    gpio_set_level(datalink_pin, 1);
    enterState(STATE_TX_ABORTED);
    xQueueReset(s_datalink_tx_queue);
    rxByteInfo.byte = COLLISION_BYTE;
    xQueueSendToBackFromISR(s_datalink_rx_queue, &rxByteInfo, NULL);
    rxByteInfo.deltaT = 0;
    bitCount = 0;
}

static void datalink_tmr_isr(void * arg)
{
    TIMER_ISR_BIT_CLR();
    if(rxByteInfo.deltaT < 0xFFFF) rxByteInfo.deltaT++;

    switch(s_state)
    {
    case STATE_RX:
        if( 0 == gpio_get_level(datalink_pin)) rxByteInfo.byte |= (0x80 >> bitCount);
        if(bitCount < 8) bitCount++;
        else
        {
            enterState(STATE_IDLE);
            rxByteInfo.deltaT -= 8; //Substract current byte duration
            xQueueSendToBackFromISR(s_datalink_rx_queue, &rxByteInfo, NULL);
            rxByteInfo.deltaT = 0;
            rxByteInfo.byte = 0x00;
            bitCount = 0;
        }
        break;

    case STATE_IDLE:
        if(rxByteInfo.deltaT > WAIT_NO_RX_BIT_BEFORE_TX &&
                uxQueueMessagesWaitingFromISR(s_datalink_tx_queue) > 0)
        {
            enterState(STATE_TX);
        }
        break;

    case STATE_TX:
    {
        if(bitCount != 0)
        {
            int expectedLevel = ((txBuffer & 0x80) ? 0 : 1);
            ets_delay_us(DELAY_WRITE_READ_PIN_US); //Needed for pin level to actually change
            int actualLevel = gpio_get_level(datalink_pin);
            if(actualLevel != expectedLevel)
            {
                onCollisionDetected();
                break;
            }
            txBuffer <<= 1;
            if(bitCount >=8 ) bitCount = 0;
        }

        if((bitCount == 0) &&
                pdTRUE != xQueueReceiveFromISR(s_datalink_tx_queue, &txBuffer, NULL))
        {
            gpio_set_level(datalink_pin, 1);
            ets_delay_us(DELAY_WRITE_READ_PIN_US); //Needed for pin level to actually change
            int actualLevel = gpio_get_level(datalink_pin);
            if(actualLevel != 1) onCollisionDetected();
            else enterState(STATE_IDLE);
        }
        else
        {
            int expectedLevel = ((txBuffer & 0x80) ? 0 : 1);
            gpio_set_level(datalink_pin, expectedLevel);
            ets_delay_us(DELAY_WRITE_READ_PIN_US); //Needed for pin level to actually change
            int actualLevel = gpio_get_level(datalink_pin);

            if(actualLevel == expectedLevel) bitCount++;
            else onCollisionDetected();
        }

        break;
    }

    case STATE_TX_ABORTED:
        if(rxByteInfo.deltaT > WAIT_BIT_ON_COLLISION) enterState(STATE_IDLE);
        break;

    default:
        ESP_LOGE(DEBUG_TAG, "Unhandled case in %s", __func__);
        break;
    }

    rescheduleAlarm(FROM_ALARM_VALUE, datalink_ticks_full_bit);         //Schedule alarm for next bit

}

static uint8_t transmit( const uint8_t *buffer, const uint8_t size)
{
    uint8_t acceptedBytes = 0;

    while(acceptedBytes < size &&
          s_state != STATE_TX_ABORTED &&
          s_datalink_tx_queue &&
          (pdTRUE == xQueueSendToBack(s_datalink_tx_queue, &buffer[acceptedBytes], 0)))
    {
        acceptedBytes++;
    }

    return acceptedBytes;
}

typedef struct
{
    uint8_t cmdByte;
    esp_avrc_pt_cmd_t action;
    char label[16];
} DatalinkCommand_t;


static DatalinkCommand_t knownCommands[] =
{
    { 0xAB          , ESP_AVRC_PT_CMD_PLAY        , "PLAY"        },
    { 0xB5          , ESP_AVRC_PT_CMD_STOP        , "STOP"        },
    { 0x99          , ESP_AVRC_PT_CMD_FORWARD     , "NEXT"        },
    { 0xB3          , ESP_AVRC_PT_CMD_BACKWARD    , "PREV"        },
    { 0xC2          , ESP_AVRC_PT_CMD_STOP        , "OFF"         },
    { 0xB1          , ESP_AVRC_PT_CMD_FAST_FORWARD, "FAST FWD"    },
    { 0xAF          , ESP_AVRC_PT_CMD_REWIND      , "REWIND"      },
    { CMDLIST_END   , 0                           , ""            }
};

static DatalinkCommand_t* getMatchingCommand(const uint8_t cmdByte)
{
    for(DatalinkCommand_t *cmdPtr = knownCommands;
        cmdPtr->cmdByte != CMDLIST_END;
        cmdPtr++)
    {
        if(cmdByte == cmdPtr->cmdByte) return cmdPtr;
    }
    return NULL;
}

#ifdef TX_DEBUG_PATTERN

static void datalink_task_handler(void* arg)
{
    ESP_LOGI(DATALINK_TAG, "Started datalink task");

    while(1)
    {
        uint8_t bytesToTransmitTest[] = {0xAA, 0xF0, 0x55};
        uint8_t successBytes = transmit(bytesToTransmitTest, sizeof(bytesToTransmitTest));
        if(successBytes == sizeof(bytesToTransmitTest))
        {
            ESP_LOGI(DATALINK_TAG, "DEBUG pattern sent %d bytes", successBytes);
        }
        else
        {
            ESP_LOGE(DATALINK_TAG, "ERROR sending DEBUG pattern, sent %d bytes", successBytes);
        }
        vTaskDelay(200 / portTICK_PERIOD_MS);
    }
}

#else

static void datalink_task_handler(void* arg)
{
    ESP_LOGI(DEBUG_TAG, "Started datalink task");
    ByteInfo_t rxByteInfo;
    uint8_t prevByte = 0x00;

    while(1)
    {
        if(pdTRUE == xQueueReceive(s_datalink_rx_queue, &rxByteInfo, (portTickType)portMAX_DELAY))
        {
            if(rxByteInfo.deltaT < 16 && rxByteInfo.byte == prevByte)
            {
                ESP_LOGD(DEBUG_TAG, "Ignoring duplicate command byte");
            }
            else
            {
                ESP_LOGI(DEBUG_TAG, "Rx : 0x%X, dT : %d", rxByteInfo.byte, rxByteInfo.deltaT);
                DatalinkCommand_t* cmdPtr = getMatchingCommand(rxByteInfo.byte);

                if(cmdPtr)
                {
                    ESP_LOGI(DEBUG_TAG, "Performing action : %s", cmdPtr->label);
                    esp_avrc_ct_send_passthrough_cmd(0, cmdPtr->action, ESP_AVRC_PT_CMD_STATE_PRESSED);
                    esp_avrc_ct_send_passthrough_cmd(1, cmdPtr->action, ESP_AVRC_PT_CMD_STATE_RELEASED);
                }
                prevByte = rxByteInfo.byte;
            }
        }
    }
}

#endif


#ifdef DEBUG_STATES
void debug_states_task_handler(void* arg)
{
    ESP_LOGI(DEBUG_TAG, "Started datalink debug state task");
    while(1)
    {
        AsynchronousStates_t state;
        if(pdTRUE == xQueueReceive(s_debug_states_queue, &state, (portTickType)portMAX_DELAY))
        {
            ESP_LOGI(DEBUG_TAG, "DEBUG STATE QUEUE : %d =  %s", state, stateStrings[state]);
        }
    }
}
#endif

void datalink_setState(enum Datalink_State state)
{
    transmit((uint8_t*)&state, 1);
}

void datalink_start( void )
{
    //Internal State Variable
    s_state = STATE_IDLE;
    bitCount = 0;
    rxByteInfo.byte = 0x00;
    rxByteInfo.deltaT = 0xFFFF;

    //IO setup
    gpio_config_t pin_conf;
    pin_conf.intr_type = GPIO_PIN_INTR_NEGEDGE;
    pin_conf.mode = GPIO_MODE_INPUT_OUTPUT_OD;
    pin_conf.pin_bit_mask = 1ULL << datalink_pin;
    pin_conf.pull_down_en = 0;
    pin_conf.pull_up_en = 1;
    gpio_config(&pin_conf);

    gpio_set_level(datalink_pin, true);

    //timer setup
    timer_config_t tmr_conf;
    tmr_conf.divider = datalink_tmr_div;
    tmr_conf.counter_dir = TIMER_COUNT_UP;
    tmr_conf.counter_en = false;
    tmr_conf.alarm_en = TIMER_ALARM_DIS;
    tmr_conf.intr_type = TIMER_INTR_LEVEL;
    tmr_conf.auto_reload = false;
    timer_init(datalink_tmr_grp, datalink_tmr_idx, &tmr_conf);

    timer_set_counter_value(datalink_tmr_grp, datalink_tmr_idx, 0x00000000ULL);
    timer_disable_intr(datalink_tmr_grp, datalink_tmr_idx);
    timer_isr_register(datalink_tmr_grp, datalink_tmr_idx, datalink_tmr_isr,
                       NULL, 0, NULL);
    timer_enable_intr(datalink_tmr_grp, datalink_tmr_idx);
    timer_start(datalink_tmr_grp, datalink_tmr_idx);


    //External interrupt
    gpio_isr_handler_add(datalink_pin, datalink_pin_isr, NULL);

    s_datalink_rx_queue = xQueueCreate(6, sizeof(ByteInfo_t));
    s_datalink_tx_queue = xQueueCreate(12, sizeof(ByteInfo_t));

#ifdef DEBUG_STATES
    s_debug_states_queue = xQueueCreate(10, sizeof(AsynchronousStates_t));
    xTaskCreate(debug_states_task_handler, "DatalinkDebugState", 4096, NULL, configMAX_PRIORITIES - 2, NULL);
#endif

    enterState(STATE_IDLE);

    xTaskCreate(datalink_task_handler, "Datalink", 4096, NULL, configMAX_PRIORITIES - 3, NULL);

    rescheduleAlarm(FROM_COUNTER_VALUE, datalink_ticks_half_bit);
}

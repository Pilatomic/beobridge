#include "esp_types.h"
#include "beolink.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "datalink.h"

#define BEOLINK_DBG_TAG               "BEOLINK"
#define LED_GPIO    (33)

enum BEOLINK_MSG_TYPES { BEOLINK_LINK_STATUS, BEOLINK_PLAY_STATUS, BEOLINK_CURRENT_TRACK, BEOLINK_TOTAL_TRACK, BEOLINK_PLAYING_TIME};


typedef struct {
    uint16_t type;
    uint16_t param;
} beolink_msg_t;

static xQueueHandle s_beolink_task_queue = NULL;

static void beolink_push_event(uint16_t type, uint16_t param)
{
    //ESP_LOGI(BEOLINK_DBG_TAG, "%s xQueue send %d, %d", __func__, type, param);

    beolink_msg_t msg;
    msg.type = type;
    msg.param = param;

    if (xQueueSend(s_beolink_task_queue, &msg, 10 / portTICK_RATE_MS) != pdTRUE) {
        ESP_LOGE(BEOLINK_DBG_TAG, "%s xQueue send failed", __func__);
    }
}

void beolink_onA2dAudioStateEvt(uint8_t audioState)
{
    beolink_push_event(BEOLINK_PLAY_STATUS, audioState == ESP_A2D_AUDIO_STATE_STARTED);
}

void beolink_onA2dConnStateEvt(uint8_t connState)
{
    beolink_push_event(BEOLINK_LINK_STATUS, connState == ESP_A2D_CONNECTION_STATE_CONNECTED);
}

void beolink_onAvrcMetadataEvt(uint8_t attr_id, uint8_t *attr_text)
{
    uint16_t type = 0;
    uint16_t value = 0;
    switch(attr_id)
    {
    case ESP_AVRC_MD_ATTR_TRACK_NUM:
        type = BEOLINK_CURRENT_TRACK;
        break;
    case ESP_AVRC_MD_ATTR_NUM_TRACKS:
        type = BEOLINK_TOTAL_TRACK;
        break;
    case ESP_AVRC_MD_ATTR_PLAYING_TIME:
        type = BEOLINK_PLAYING_TIME;
        break;
    default:
        return;
    }

    value = strtol((char*)attr_text, NULL, 10);

    beolink_push_event(type,value);
}


enum SystemStateEnum { System_Started, System_NotConnected, System_Connected, System_Playing };

static void beolink_task_handler(void* arg)
{
    /*uint32_t io_num;
    for(;;) {
        if(xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
            printf("GPIO[%d] intr, val: %d\n", io_num, gpio_get_level(io_num));
        }
    }*/

    uint16_t systemState = System_NotConnected;
    uint16_t prevSystemState = System_Started;
    bool ledState = false;
    gpio_pad_select_gpio(LED_GPIO);
    gpio_set_direction(LED_GPIO, GPIO_MODE_OUTPUT);

    while(1) {
        beolink_msg_t msg;
        while(xQueueReceive(s_beolink_task_queue, &msg, 10 / portTICK_RATE_MS)) {
            if(msg.type == BEOLINK_LINK_STATUS)
            {
                systemState = msg.param ? System_Connected : System_NotConnected;
            }
            else if(msg.type == BEOLINK_PLAY_STATUS)
            {
                systemState = msg.param ? System_Playing : System_Connected;
            }
            //ESP_LOGI(BEOLINK_DBG_TAG, "%s xQueue receive %d, %d", __func__, msg.type, msg.param);
        }
        //ESP_LOGI(BEOLINK_DBG_TAG, "%s Loop playing: %d", __func__, playing);

       /* if(systemState != prevSystemState)
        {
            switch(systemState)
            {
            case System_NotConnected:
                datalink_setState(Datalink_NoSource);
                ESP_LOGI(BEOLINK_DBG_TAG, "NOT CONNECTED");
                break;
            case System_Connected:
                datalink_setState(Datalink_Stopped);
                ESP_LOGI(BEOLINK_DBG_TAG, "STOPPED");
                break;
            case System_Playing:
                datalink_setState(Datalink_Playing);
                ESP_LOGI(BEOLINK_DBG_TAG, "PLAYING");
                break;
            default:
                break;
            }
            prevSystemState = systemState;
        }*/
#warning do not transmit for the moment, need to check that everything ends up properly on the bus

        switch(systemState)
        {
        case System_NotConnected:
            ledState = false;
            break;
        case System_Connected:
            ledState = true;
            break;
        case System_Playing:
            ledState = !ledState;
            break;
        default:
            break;
        }

        gpio_set_level(LED_GPIO,ledState);

        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
}


void beolink_task_start()
{
    s_beolink_task_queue = xQueueCreate(10, sizeof(beolink_msg_t));
    xTaskCreate(beolink_task_handler, "Beolink", 4096, NULL, configMAX_PRIORITIES - 3, NULL);
}


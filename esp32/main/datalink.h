#ifndef DATALINK_H
#define DATALINK_H

#include "esp_types.h"

enum Datalink_State
{
    Datalink_NoSource = 0xC5,
    Datalink_Playing = 0xB7,
    Datalink_Stopped = 0xB4
};

void datalink_start( void );
void datalink_setState(enum Datalink_State state);

#endif

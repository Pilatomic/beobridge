#ifndef I2SOUT_H
#define I2SOUT_H

#include <stdint.h>

void i2sout_data_cb(const uint8_t *data_in, uint32_t len);

void i2sout_setclk(uint32_t rate);

void i2sout_init();


#endif

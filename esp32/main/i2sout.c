#include "i2sout.h"
#include "bt_app_av.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "driver/i2s.h"

#define GAIN                        (40000)
#define OUTPUT_BITS_PER_SAMPLE      (32)
#define OUTPUT_BUF_SIZE             (256 * OUTPUT_BITS_PER_SAMPLE)
#define I2SOUT_TAG                  "I2SOUT"

static uint8_t buf[OUTPUT_BUF_SIZE] = {0};

void i2sout_init()
{
    i2s_config_t i2s_config = {
        .mode = I2S_MODE_MASTER | I2S_MODE_TX,                                  // Only TX
        .sample_rate = 44100,
        .bits_per_sample = OUTPUT_BITS_PER_SAMPLE,
        .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,                           //2-channels
        .communication_format = I2S_COMM_FORMAT_STAND_I2S,
        .dma_buf_count = 8,
        .dma_buf_len = 1024,
        .intr_alloc_flags = 0,                                                  //Default interrupt priority
        .tx_desc_auto_clear = true,                                             //Auto clear tx descriptor on underflow
        .use_apll = true
    };


    i2s_driver_install(0, &i2s_config, 0, NULL);
    i2s_pin_config_t pin_config = {
        .bck_io_num = CONFIG_I2S_BCK_PIN,
        .ws_io_num = CONFIG_I2S_LRCK_PIN,
        .data_out_num = CONFIG_I2S_DATA_PIN,
        .data_in_num = -1                                                       //Not used
    };

    i2s_set_pin(0, &pin_config);
}

void i2sout_setclk(uint32_t rate)
{
    i2s_set_clk(0, rate, OUTPUT_BITS_PER_SAMPLE, 2);
}

void i2sout_data_cb(const uint8_t *samples, uint32_t len)
{
    //Convert incoming 16 bit data to 32 bits and attenuate
    // INPUT AND OUTPUT SAMPLE WIDTH ARE HARDCODED !!!
    int32_t *data_out = (int32_t*) buf;
    int16_t *data_in = (int16_t*) samples;
    size_t sampleCount = len / sizeof(*data_in);
    size_t bytes_written;

    for(uint32_t i = 0 ; i < sampleCount  ; i++)
    {
        *data_out =  ((int32_t) GAIN) * ((int32_t) *data_in);

        data_out += 1;
        data_in += 1;
    }

    i2s_write(0, buf, sampleCount * sizeof(*data_out), &bytes_written, portMAX_DELAY);
}

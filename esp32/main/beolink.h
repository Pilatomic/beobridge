#ifndef BEOLINK_H
#define BEOLINK_H

#include <stdint.h>
#include "esp_a2dp_api.h"
#include "esp_avrc_api.h"

void beolink_onA2dAudioStateEvt(uint8_t audioState);
void beolink_onA2dConnStateEvt(uint8_t connState);
void beolink_onAvrcMetadataEvt(uint8_t attr_id, uint8_t *attr_text);

void beolink_task_start();


#endif // BEOLINK_H
